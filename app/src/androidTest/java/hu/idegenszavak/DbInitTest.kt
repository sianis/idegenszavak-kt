package hu.idegenszavak

import android.content.Context
import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import com.squareup.moshi.Moshi
import hu.idegenszavak.model.Word
import hu.idegenszavak.model.db.AppDatabase
import hu.idegenszavak.model.db.DbInitializer
import hu.idegenszavak.model.db.WordRepository
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.koin.core.context.loadKoinModules
import org.koin.core.context.unloadKoinModules
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.koin.test.get
import org.koin.test.inject

@RunWith(JUnit4::class)
class DbInitTest : KoinTest {

    private val roomTestModule = module {
        single(override = true) {
            Room.inMemoryDatabaseBuilder(get(), AppDatabase::class.java).allowMainThreadQueries().build()
        }
        single<DbInitializer>(override = true) { MockDbInitializer() }
    }

    @Test
    fun initDbTest() {
        val repository by inject<WordRepository>()
        val dbInitializer: DbInitializer by inject()
        val list =
            dbInitializer.parseAssetJson(InstrumentationRegistry.getInstrumentation().targetContext, get())
        assertTrue(dbInitializer.isDbInitNecessary(repository))
        assertEquals(1, list.size)
        assertEquals(0, repository.getCount())
        dbInitializer.saveWords(repository, list)
        assertEquals(1, repository.getCount())
    }

    @Before
    fun before() {
        loadKoinModules(roomTestModule)
    }

    @After
    fun after() {
        unloadKoinModules(roomTestModule)
    }
}

class MockDbInitializer : DbInitializer() {

    override fun isDbInitNecessary(wordRepository: WordRepository): Boolean {
        return true
    }

    override fun parseAssetJson(context: Context, moshi: Moshi): List<Word> {
        return listOf(Word(0, "one", description = "description"))
    }
}
