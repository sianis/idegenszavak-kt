package hu.idegenszavak

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import androidx.work.ListenableWorker
import androidx.work.testing.TestListenableWorkerBuilder
import hu.idegenszavak.api.DbSyncWork
import hu.idegenszavak.model.db.WordRepository
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.koin.test.KoinTest
import org.koin.test.inject

@RunWith(JUnit4::class)
class DbSyncWorkTest : KoinTest {

    private val repository by inject<WordRepository>()
    private lateinit var context: Context

    @Before
    fun setup() {
        context = ApplicationProvider.getApplicationContext()
    }

    @Test
    fun testMyWork() {
        // Get the ListenableWorker
        val worker = TestListenableWorkerBuilder<DbSyncWork>(context).build()
        // Run the worker synchronously
        val result = worker.startWork().get()
        assertEquals(ListenableWorker.Result.success(), result)
        assertEquals(10047, repository.getCount())
    }
}