package hu.idegenszavak

import android.view.View
import android.widget.TextView
import androidx.databinding.BindingAdapter

@BindingAdapter(value = ["originString"])
fun bindOriginOrGone(textView: TextView, value: String?) {
    if (!value.isNullOrBlank()) {
        textView.text = textView.context.getString(R.string.wordOrigin, value)
        textView.visibility = View.VISIBLE
    } else {
        textView.text = null
        textView.visibility = View.GONE
    }
}

@BindingAdapter(value = ["sourceString"])
fun bindSourceOrGone(textView: TextView, value: String?) {
    if (!value.isNullOrBlank()) {
        textView.text = textView.context.getString(R.string.wordSource, value)
        textView.visibility = View.VISIBLE
    } else {
        textView.text = null
        textView.visibility = View.GONE
    }
}

@BindingAdapter(value = ["sentByString"])
fun bindSentByStringOrGone(textView: TextView, value: String?) {
    if (!value.isNullOrBlank()) {
        textView.text = textView.context.getString(R.string.wordSentBy, value)
        textView.visibility = View.VISIBLE
    } else {
        textView.text = null
        textView.visibility = View.GONE
    }
}