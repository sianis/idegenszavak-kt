package hu.idegenszavak

import android.app.Application
import androidx.room.Room
import androidx.work.*
import com.squareup.moshi.Moshi
import hu.idegenszavak.api.DbSyncWork
import hu.idegenszavak.api.IdegenSzavakApi
import hu.idegenszavak.model.db.*
import hu.idegenszavak.ui.main.MainViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

class IdegenSzavakApp : Application() {

    private val appModule = module {
        single { Room.databaseBuilder(get(), AppDatabase::class.java, BuildConfig.DbName).build() }
        factory { get<AppDatabase>().wordDao() }
        factory<WordRepository> { WordRepositoryImpl(get()) }
        single {
            Retrofit.Builder().baseUrl(BuildConfig.ApiUrl)
                .addConverterFactory(MoshiConverterFactory.create()).build()
        }
        single<IdegenSzavakApi> { get<Retrofit>().create(IdegenSzavakApi::class.java) }
        single { Moshi.Builder().build() }
        factory<DbInitializer> { DbInitializerImpl() }
        viewModel { MainViewModel(get()) }
    }

    override fun onCreate() {
        super.onCreate()
        startKoin {
            if (BuildConfig.DEBUG) {
                androidLogger()
            }
            androidContext(this@IdegenSzavakApp)
            modules(appModule)
        }
        scheduleWork()
    }

    private fun scheduleWork() {
        val dbSyncWork = PeriodicWorkRequestBuilder<DbSyncWork>(1, TimeUnit.DAYS)
            .setInitialDelay(5, TimeUnit.MINUTES)
            .setConstraints(
                Constraints.Builder()
                    .setRequiredNetworkType(NetworkType.UNMETERED)
                    .setRequiresDeviceIdle(true)
                    .build()
            )
            .build()
        WorkManager.getInstance(this)
            .enqueueUniquePeriodicWork(DbSyncWork::class.java.name, ExistingPeriodicWorkPolicy.REPLACE, dbSyncWork)
    }


}