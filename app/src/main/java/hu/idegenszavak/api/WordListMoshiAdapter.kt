package hu.idegenszavak.api

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonReader
import com.squareup.moshi.Moshi
import hu.idegenszavak.model.Word
import java.text.Normalizer

class WordListMoshiAdapter(moshi: Moshi) {

    private val personAdapter: JsonAdapter<Word> = moshi.adapter(Word::class.java)

    fun parse(reader: JsonReader): List<Word> {
        return reader.readArrayToList {
            //Custom parsing for normalizing words in json deserialization time
            personAdapter.fromJson(reader)?.apply { wordNormalized = Normalizer.normalize(word, Normalizer.Form.NFD) }
        }
    }

    private inline fun <T : Any> JsonReader.readArrayToList(body: () -> T?): List<T> {
        val result = mutableListOf<T>()
        beginArray()
        while (hasNext()) {
            body()?.let { result.add(it) }
        }
        endArray()
        return result
    }
}