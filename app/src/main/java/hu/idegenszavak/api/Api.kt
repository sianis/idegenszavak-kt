package hu.idegenszavak.api

import hu.idegenszavak.BuildConfig
import hu.idegenszavak.model.Word
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers

interface IdegenSzavakApi {

    @Headers(BuildConfig.ApiAuthHeader)
    @GET(value = "adatbazis/lekerdez/format/json")
    fun listWords(): Call<List<Word>>
}