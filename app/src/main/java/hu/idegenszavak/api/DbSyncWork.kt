package hu.idegenszavak.api

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import hu.idegenszavak.model.db.WordRepository
import org.koin.core.KoinComponent
import org.koin.core.inject

class DbSyncWork(context: Context, params: WorkerParameters) :
    CoroutineWorker(context, params), KoinComponent {

    override suspend fun doWork(): Result {
        return try {
            val idegenSzavakApi: IdegenSzavakApi by inject()
            val response = idegenSzavakApi.listWords().execute()
            if (response.isSuccessful) {
                response.body()?.run {
                    val wordRepository: WordRepository by inject()
                    wordRepository.syncDatabase(this)
                    Result.success()
                } ?: Result.retry()
            } else {
                Result.retry()
            }
        } catch (error: Throwable) {
            error.printStackTrace()
            Result.failure()
        }
    }
}
