package hu.idegenszavak.model.db

import androidx.lifecycle.LiveData
import hu.idegenszavak.model.Word

interface WordRepository {
    fun getAllLive(): LiveData<List<Word>>
    fun getCount(): Long
    fun saveWords(words: List<Word>)
    suspend fun syncDatabase(words: List<Word>)
}

class WordRepositoryImpl(private val wordDao: WordDao) : WordRepository {
    override fun getAllLive(): LiveData<List<Word>> {
        return wordDao.getAllLive()
    }

    override fun getCount(): Long {
        return wordDao.getCount()
    }

    override fun saveWords(words: List<Word>) {
        wordDao.insertAll(words)
    }

    override suspend fun syncDatabase(words: List<Word>) {
        wordDao.syncDatabase(words)
    }
}