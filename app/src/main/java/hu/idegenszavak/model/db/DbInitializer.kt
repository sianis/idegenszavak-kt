package hu.idegenszavak.model.db

import android.content.Context
import com.squareup.moshi.JsonReader
import com.squareup.moshi.Moshi
import hu.idegenszavak.api.WordListMoshiAdapter
import hu.idegenszavak.model.Word
import okio.Okio

abstract class DbInitializer {

    abstract fun isDbInitNecessary(wordRepository: WordRepository): Boolean
    abstract fun parseAssetJson(context: Context, moshi: Moshi): List<Word>
    fun saveWords(wordRepository: WordRepository, words: List<Word>) {
        wordRepository.saveWords(words)
    }
}

class DbInitializerImpl : DbInitializer() {

    override fun isDbInitNecessary(wordRepository: WordRepository): Boolean {
        return wordRepository.getCount() == 0L
    }

    override fun parseAssetJson(context: Context, moshi: Moshi): List<Word> {
        Okio.source(context.assets.open("db.json")).use { source ->
            Okio.buffer(source).use { buffer ->
                return WordListMoshiAdapter(moshi).parse(JsonReader.of(buffer))
            }
        }
    }
}