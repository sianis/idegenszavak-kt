package hu.idegenszavak.model.db

import androidx.lifecycle.LiveData
import androidx.room.*
import hu.idegenszavak.model.WORDS_TABLE
import hu.idegenszavak.model.Word

@Dao
abstract class WordDao {
    @Query("SELECT * FROM $WORDS_TABLE ORDER BY id DESC")
    abstract fun getAllLive(): LiveData<List<Word>>

    @Query("SELECT * FROM $WORDS_TABLE")
    abstract fun getAll(): List<Word>

    @Query("SELECT COUNT(id) FROM $WORDS_TABLE")
    abstract fun getCount(): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertAll(words: List<Word>)

    @Delete
    abstract fun delete(word: Word)

    @Transaction
    open suspend fun syncDatabase(actualWords: List<Word>) {
        val ids = actualWords.map { it.id }
        val notIn = getAll().filterNot { ids.contains(it.id) }
        notIn.forEach { delete(it) }
        insertAll(actualWords)
    }
}

@Database(entities = [Word::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun wordDao(): WordDao
}