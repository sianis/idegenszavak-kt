package hu.idegenszavak.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json

const val WORDS_TABLE = "words"

@Entity(tableName = WORDS_TABLE)
data class Word(
    @PrimaryKey
    @field:Json(name = "id")
    val id: Long,
    @field:Json(name = "szo")
    val word: String,
    var wordNormalized: String? = null,
    @field:Json(name = "magyarazat")
    val description: String,
    @field:Json(name = "eredet")
    val origin: String? = null,
    @field:Json(name = "forras")
    val source: String? = null,
    val sentBy: String? = null
)