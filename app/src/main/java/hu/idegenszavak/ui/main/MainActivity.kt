package hu.idegenszavak.ui.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import hu.idegenszavak.R
import hu.idegenszavak.model.db.DbInitializer
import org.jetbrains.anko.doAsync
import org.koin.android.ext.android.get
import org.koin.android.ext.android.inject

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initDbWhenNecessary()
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .add(R.id.content, obtainMainFragment())
                .commit()
            obtainMainFragment()
        }
    }

    private fun initDbWhenNecessary() {
        doAsync {
            val dbInitializer: DbInitializer by inject()
            if (dbInitializer.isDbInitNecessary(get())) {
                dbInitializer.saveWords(get(), dbInitializer.parseAssetJson(baseContext, get()))
            }
        }
    }

    private fun obtainMainFragment(): MainFragment {
        return supportFragmentManager.findFragmentById(R.id.content) as MainFragment? ?: MainFragment()
    }
}
