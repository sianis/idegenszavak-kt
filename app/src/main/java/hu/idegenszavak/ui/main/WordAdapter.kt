package hu.idegenszavak.ui.main

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import hu.idegenszavak.databinding.WordCardItemBinding
import hu.idegenszavak.model.Word

class WordAdapter(private val context: Context, private val words: List<Word>) :
    RecyclerView.Adapter<WordViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WordViewHolder {
        return WordViewHolder(
            WordCardItemBinding.inflate(
                LayoutInflater.from(
                    context
                ), parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return words.size
    }

    override fun onBindViewHolder(holder: WordViewHolder, position: Int) {
        holder.bind(words[position])
    }
}

class WordViewHolder(private val binding: WordCardItemBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(word: Word) {
        binding.item = word
        binding.executePendingBindings()
    }
}