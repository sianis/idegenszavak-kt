package hu.idegenszavak.ui.main

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import hu.idegenszavak.R
import hu.idegenszavak.databinding.FragmentMainBinding
import org.jetbrains.anko.design.indefiniteSnackbar
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainFragment : Fragment() {

    private val mainViewModel: MainViewModel by viewModel()
    private lateinit var binding: FragmentMainBinding
    private lateinit var loadingSnackbar: Snackbar

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (!::binding.isInitialized) {
            binding = FragmentMainBinding.inflate(inflater, container, false)
            binding.recyclerView.layoutManager = LinearLayoutManager(context)
            binding.recyclerView.setHasFixedSize(false)
        }

        mainViewModel.words.observe(this, Observer { list ->
            if (list.isNullOrEmpty()) {
                showLoader()
            } else {
                binding.recyclerView.adapter = WordAdapter(binding.recyclerView.context, list)
                hideLoader()
            }
        })
        setHasOptionsMenu(true)
        return binding.root
    }

    private fun hideLoader() {
        if (::loadingSnackbar.isInitialized && loadingSnackbar.isShown) {
            loadingSnackbar.dismiss()
        }
    }

    private fun showLoader() {
        if (!::loadingSnackbar.isInitialized) {
            loadingSnackbar = binding.root.indefiniteSnackbar(R.string.loaddata)
                .apply {
                    setAction(android.R.string.ok) { hideLoader() }
                }
        }
        if (!loadingSnackbar.isShown) {
            loadingSnackbar.show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.main, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.newest -> mainViewModel.sortSelected(Sort.NEWEST)
            R.id.oldest -> mainViewModel.sortSelected(Sort.OLDEST)
            R.id.abc -> mainViewModel.sortSelected(Sort.ALPHABETIC)
            R.id.zxy -> mainViewModel.sortSelected(Sort.ALPHABETIC_INVERSE)
            R.id.random -> mainViewModel.sortSelected(Sort.RANDOM)
        }
        return super.onOptionsItemSelected(item)
    }
}

