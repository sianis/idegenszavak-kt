package hu.idegenszavak.ui.main

import androidx.annotation.IntDef
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import hu.idegenszavak.model.Word
import hu.idegenszavak.model.db.WordRepository
import org.jetbrains.anko.doAsync

class MainViewModel(wordRepository: WordRepository) : ViewModel() {
    private val allWordLive = wordRepository.getAllLive()
    val words = MediatorLiveData<List<Word>>()
    private var sort = Sort.NEWEST

    init {
        words.addSource(allWordLive) { result: List<Word>? ->
            doAsync {
                result?.let {
                    sortedList(it, sort).run {
                        words.postValue(this)
                    }
                }
            }
        }
    }

    private fun sortedList(words: List<Word>, sort: Int): List<Word> {
        return when (sort) {
            Sort.NEWEST -> words.sortedByDescending { it.id }
            Sort.OLDEST -> words.sortedBy { it.id }
            Sort.ALPHABETIC -> words.sortedWith(compareBy(String.CASE_INSENSITIVE_ORDER) {
                it.wordNormalized ?: it.word
            })
            Sort.ALPHABETIC_INVERSE -> words.sortedWith(compareByDescending(String.CASE_INSENSITIVE_ORDER) {
                it.wordNormalized ?: it.word
            })
            else -> words.shuffled()
        }
    }

    fun sortSelected(@Sort sort: Int) {
        if (sort == Sort.RANDOM || this.sort != sort) {
            saveAndSortList(sort)
        }
    }

    private fun saveAndSortList(sort: Int) = allWordLive.value?.let {
        words.value = sortedList(it, sort)
    }.also { this.sort = sort }
}

@Retention(AnnotationRetention.SOURCE)
@IntDef(
    Sort.NEWEST,
    Sort.OLDEST,
    Sort.ALPHABETIC,
    Sort.ALPHABETIC_INVERSE,
    Sort.RANDOM
)
annotation class Sort {
    companion object {
        const val NEWEST = 0
        const val OLDEST = 1
        const val ALPHABETIC = 2
        const val ALPHABETIC_INVERSE = 3
        const val RANDOM = 4
    }
}