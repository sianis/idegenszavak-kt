package hu.idegenszavak

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jraska.livedata.TestObserver
import hu.idegenszavak.model.Word
import hu.idegenszavak.model.db.WordRepository
import hu.idegenszavak.ui.main.MainViewModel
import hu.idegenszavak.ui.main.Sort
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class MainViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var mainViewModel: MainViewModel
    private lateinit var testObserver: TestObserver<List<Word>>

    @Before
    fun init() {
        mainViewModel = MainViewModel(MockWordRepository())
        testObserver = TestObserver.test(mainViewModel.words)
            .awaitValue()
    }

    @Test
    fun testHasValue() {
        testObserver.assertHasValue()
    }

    @Test
    fun testListSize() {
        Assert.assertEquals(4, mainViewModel.words.value!!.size)
    }

    @Test
    fun testSortNewest() {
        mainViewModel.sortSelected(Sort.NEWEST)
        Assert.assertEquals(listOf(3, 2, 1, 0), mainViewModel.words.value!!.map { it.id.toInt() })
    }

    @Test
    fun testSortOldest() {
        mainViewModel.sortSelected(Sort.OLDEST)
        Assert.assertEquals(listOf(0, 1, 2, 3), mainViewModel.words.value!!.map { it.id.toInt() })
    }

    @Test
    fun testSortAlphabetical() {
        mainViewModel.sortSelected(Sort.ALPHABETIC)
        Assert.assertEquals(listOf(3, 1, 0, 2), mainViewModel.words.value!!.map { it.id.toInt() })
    }

    @Test
    fun testSortAlphabeticalRevers() {
        mainViewModel.sortSelected(Sort.ALPHABETIC_INVERSE)
        Assert.assertEquals(listOf(2, 0, 1, 3), mainViewModel.words.value!!.map { it.id.toInt() })
    }
}

class MockWordRepository : WordRepository {

    override fun getAllLive(): LiveData<List<Word>> {
        return MutableLiveData<List<Word>>().apply {
            postValue(testList)
        }
    }

    override fun getCount(): Long {
        return testList.size.toLong()
    }

    override fun saveWords(words: List<Word>) {
        //Do nothing
    }

    override suspend fun syncDatabase(words: List<Word>) {
        //Do nothing
    }
}

val testList = listOf(
    Word(0, "papundekli", description = "kartonpapír"),
    Word(1, "mitomániás", description = "Kényszeres hazudozó."),
    Word(2, "stimuláló", description = "ösztönző, ingerlő"),
    Word(3, "mantrázik", description = "Meditációs, elmélyülési szándékkal ismételget egy szót.")
)